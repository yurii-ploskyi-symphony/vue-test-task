import BlockStorageService from 'services/desktop/BlocksStorageService.js'
import defaultBlocks from 'assets/defaultBlocks.json'

export default {
    getBlocks() {
        return BlockStorageService.getBlocks()
    },
    saveBlocks(blocks) {
        const defaultHeight = 100;
        const filterById = (blocks, id) => blocks.find(block => block.id === id)
        const moveToEndEndIfNeeded = (block, oldBlocks) => {
            if (isBlockSwitchedFromInactiveToActive(block, oldBlocks)) {
                moveToEndAndSetDefaultHeight(block)
            }
        }
        const isBlockSwitchedFromInactiveToActive = (block, oldBlocks) =>
            block.isActive && !filterById(oldBlocks, block.id).isActive
        const moveToEndAndSetDefaultHeight = block => {
            const oldIndex = blocks.findIndex(oldBlock => oldBlock.id === block.id)
            const oldBlock = blocks[oldIndex]
            blocks.splice(oldIndex, 1)
            oldBlock.isActive = true
            oldBlock.height = defaultHeight
            blocks.push(oldBlock)
        }
        const oldBlocks = BlockStorageService.getBlocks();
        blocks.forEach(block => moveToEndEndIfNeeded(block, oldBlocks))
        BlockStorageService.saveBlocks(blocks)
    },
    loadDefaultBlocksIfNeeded() {
        if (!this.getBlocks()) {
            BlockStorageService.saveBlocks(defaultBlocks)
        }
    }
}