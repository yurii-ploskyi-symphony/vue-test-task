const localStorageName = 'investmentAppLocalStorage'
const localStorage = window.localStorage

export default {
    getBlocks() {
        return JSON.parse(localStorage.getItem(localStorageName))
    },
    saveBlocks(blocks) {
        localStorage.setItem(localStorageName, JSON.stringify(blocks))
    }
}