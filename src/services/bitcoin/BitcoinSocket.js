import {bitcoinUrl} from 'src/constants.js'
const parseTransactionObject = message => {
    const messageDataX = JSON.parse(message.data).x
    const out = messageDataX.out[0]
    return {
        from: out.addr,
        to: messageDataX.inputs[0].prev_out.addr,
        sum: out.value / 100000000
    }
}
const subscribeMessage = {'op': 'unconfirmed_sub'}
const unSubscribeMessage = {'op': 'unconfirmed_unsub'}
export default {
    webSocket: new WebSocket(bitcoinUrl),
    subscribe() {
        if (this.webSocket.readyState === this.webSocket.OPEN) {
            this.initialSubscribe()
        } else {
            this.webSocket.onopen = () => this.initialSubscribe()
        }
    },
    sendMessage(message) {
        this.webSocket.send(JSON.stringify(message))
    },
    initialSubscribe() {
        this.sendMessage(subscribeMessage)
    },
    unSubscribe() {
        this.sendMessage(unSubscribeMessage)
    },
    onmessage(callback) {
        this.webSocket.onmessage = message => callback(parseTransactionObject(message))
    }
}