import Vue from 'vue'
import Router from 'vue-router'

const Desktop = () => import('components/desktop/Desktop.vue')
const Bitcoin = () => import('components/bitcoin/Bitcoin.vue')
const Navigation = () => import('components/Navigation.vue')

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        name: 'navigation',
        component: Navigation
    }, {
        path: '/Desktop',
        name: 'desktop',
        component: Desktop
    }, {
        path: '/Bitcoin',
        name: 'bitcoin',
        component: Bitcoin
    }
    ]
})