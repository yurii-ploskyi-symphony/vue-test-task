const path = require('path')
const fs = require('fs')

function resolve (dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    configureWebpack: {
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
                src: resolve('src'),
                components: resolve('src/components'),
                shared: resolve('src/shared'),
                services: resolve('src/services'),
                node_modules: resolve('node_modules'),
                assets: resolve('src/assets')
            }
        }
    }
}